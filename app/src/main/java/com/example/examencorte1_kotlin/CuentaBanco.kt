package com.example.examencorte1_kotlin

class CuentaBanco {
    var numCuenta : Int=0;
    var Nombre : String="";
    var Banco : String="";
    var saldo : Float= 0.0F;

    constructor(numCuenta:Int, Nombre:String, Banco:String, saldo:Float){
        this.numCuenta = 0
        this.Nombre = ""
        this.Banco = ""
        this.saldo = 0.0F
    }

    fun depositar(ingreso : Float, saldo: Float): Float{
        return  saldo + ingreso
    }

    fun retirar(retiro : Float, saldo: Float):Float{
         var valido = false;

        if (saldo>=retiro){
            valido = true
        } else {
            valido
        }

        if (valido == true){
            return saldo - retiro
        } else{
            return saldo;
        }
    }
}

