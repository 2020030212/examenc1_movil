package com.example.examencorte1_kotlin

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog


class MainActivity : AppCompatActivity() {

    private lateinit var txtnumCuenta : EditText
    private lateinit var txtNombre : EditText
    private lateinit var txtBanco : EditText
    private lateinit var txtSaldo : EditText
    private lateinit var btnEnviar : Button
    private lateinit var btnSalir : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
    }

    private fun iniciarComponentes(){
        txtnumCuenta = findViewById(R.id.txtnumCuenta)
        txtNombre = findViewById(R.id.txtNombre)
        txtSaldo = findViewById(R.id.txtSaldo)
        txtBanco = findViewById(R.id.txtBanco)
        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun Enviar() {
        var strNombre: String
        var strSaldo: String
        var strBanco: String
        var strnumCuenta: String

        strnumCuenta = applicationContext.resources.getString(R.id.numCuenta)
        strNombre = applicationContext.resources.getString(R.string.Nombre)
        strBanco = applicationContext.resources.getString(R.string.banco)
        strSaldo = applicationContext.resources.getString(R.string.Saldo)

        if (strnumCuenta.toString().equals(txtnumCuenta.text.toString()) && strNombre.toString()
                .equals(txtNombre.text.toString()) &&
            strBanco.toString().equals(txtBanco.text.toString()) && strSaldo.toString()
                .equals(txtSaldo.text.toString())
        ) {

            var bundle = Bundle();
            bundle.putString("Nombre", txtNombre.text.toString())
            bundle.putString("Saldo", txtSaldo.text.toString())

            val intent = Intent(this@MainActivity, CuentaBancoActivity::class.java)
            intent.putExtras(bundle)

            startActivity(intent)

        } else {
            Toast.makeText(
                this.applicationContext,
                "Ingrese los datos correctos",
                Toast.LENGTH_LONG
            ).show()
        }
    }
    private fun regresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Cuenta de Banco")
        confirmar.setMessage("¿Desea salir?")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }
}